let nav = document.querySelector('.navigation__img');
let navActive = document.querySelector('.navigation__img--active');
let contextMenu = document.querySelector('.navigation__context')



nav.onclick = (()=>{
  navActive.classList.remove('invisible');
  contextMenu.style.display = 'block';
  contextMenu.classList.add('context-active');
  nav.classList.add('invisible');
});

navActive.onclick = (()=>{
  navActive.classList.add('invisible');
  contextMenu.style.display = 'none';
  nav.classList.remove('invisible');
});

